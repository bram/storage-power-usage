.PHONY: clean index pages preview venv

pages: chart1 index

preview: venv
	. venv/bin/activate; python chart1.py

index:
	envsubst < index.html > public/index.html

chart1: public/images/chart1.png

public/images/chart1.png: venv
	. venv/bin/activate; python chart1.py -f public/images/chart1.png

venv: venv/touchfile

venv/touchfile: requirements.txt
	test -d venv || python3 -m venv venv
	. venv/bin/activate; pip install -Ur requirements.txt
	touch venv/touchfile

clean:
	rm -rf venv public/images/* public/index.html
