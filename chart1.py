import pandas as pd
import matplotlib.pyplot as plt
import io
import argparse

data = pd.read_csv('chart1-storageclass-vs-power.csv')

# Extract the relevant columns from the DataFrame
storage_class = data['storage_class']
power_usage = data['Watt/TB']
color = data['color']

# Initialize the plot and set the figure size
fig, ax = plt.subplots(figsize=(10, 8))

X = list(data.iloc[:, 0])
Y = power_usage

plt.bar(X, Y, color=color)



# Set labels for the axes
ax.set_xlabel('Storage Class')
ax.set_ylabel('Power Usage (Watt / TB)')

# Set title for the plot
ax.set_title('Storage Class vs Power Usage')


parser = argparse.ArgumentParser(description='plot script')
parser.add_argument('-f', '--file', type=str, help='Path to output file')

args = parser.parse_args()
if args.file:
    plt.savefig(args.file, bbox_inches='tight')
else:
    plt.show()